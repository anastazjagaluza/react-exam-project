module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  plugins: [
    'react',
    'babel',
  ],
  parser: 'babel-eslint',
  rules: {
    'linebreak-style': 0,
    'global-require': 0,
    'eslint linebreak-style': [0, 'error', 'windows'],
    'no-console': 'off',
    'import/no-named-as-default': 0,
    'react/prop-types': 'off',
    'no-invalid-this': 0,
    'babel/no-invalid-this': 1,
    'no-undef': 'off',
    'class-methods-use-this': 'off',
    'max-len': 'off',
    'no-alert': 'off',
    'react/jsx-wrap-multilines': 'off',
  },
  root: true,
};

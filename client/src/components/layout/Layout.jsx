import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Cookies from 'js-cookie';

// STYLES
import './layout.css';

// ASSETS
import LogoLong from '../../assets/logos/Logo_2.svg';

class Layout extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  handleLogout = () => {
    // Remove accessToken for auth
    localStorage.removeItem('accessToken');
    // Remove cookie for _id
    Cookies.remove('scatcherUserId');
    // Refresh page
    window.location.reload();
  }

  render() {
    // Deconstructed Props
    const { children } = this.props;

    // * BaseClass for styling. To make unique classes. Named after compnent name
    // * Usign BEM (Block Element Modifier) naming aproach http://getbem.com/naming/
    // * fx <span class="block__elem"></span> / <div class="block block--mod">...</div>
    // * use - for longnames. eg. block-name__elem--mod / layout-nav__button--green
    const baseClass = 'layout';
    // class="signup-form_button--green"

    return (
      <div>
        <header>
          <nav className={`${baseClass}-nav`}>
            <img className={`${baseClass}-nav__logo`} src={LogoLong} alt="Logo" />
            <ul className={`${baseClass}-nav__ul`}>
              <li className={`${baseClass}-nav__li`}><Link className={`${baseClass}-nav__link`} to="/page1"> page1</Link></li>
              <li className={`${baseClass}-nav__li`}><Link className={`${baseClass}-nav__link`} to="/page2"> page2</Link></li>
              <li className={`${baseClass}-nav__li`}><button className={`${baseClass}-nav__button`} onClick={this.handleLogout} type="button">Log Out</button></li>
            </ul>
          </nav>
        </header>
        <main>
          {children}
        </main>
      </div>
    );
  }
}

export default Layout;

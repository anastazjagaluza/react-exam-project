import React, { Component } from 'react';

// STYLES
import './canvas.css';

// assets
import DeleteIcon from './delete.svg';
import SaveIcon from './save.svg';

class Canvas extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentColor: 'black',
      currentStroke: 1,
      allColors: ['black', '#545454', '#252847', '#045156', '#086F50', '#14BC82', '#A6D26A', '#F3B951', '#E76839', '#E50F45', '#9B2B5A', '#6C045D'],
      allStrokes: [1, 2, 3, 4, 5, 6],
      points: [],
    };
    // this is a reference to the canvas, it works kinda like document.querySelector almost
    this.canvas = React.createRef();
  }

  // This lifecycle method will be used to save drawing points in the database. The reason the saving is in here and
  // not in any regular function is, that setState in react is asynchronous and if you don't wait for the component to
  // update, there will appear the reace condition and the last drawing points are not saved.
  componentDidUpdate() {
    const { points } = this.state;
    if (points.length === 0) return;
    if (points[points.length - 1].mode === 'end') {
      // localstorage for now is just to test, later it will be a post request to the db
      localStorage.setItem('drawingpoints', JSON.stringify(points));
    }
  }

  onComponentWillMount() {
    // setting canvas width and height in css or just inline causes the offset to be wrong,
    // somehow then react doesn't calculate the mouse event correctly in relation to the canvas
    this.canvas.current.width = 0.7 * window.innerWidth;
    this.canvas.current.height = 0.7 * window.innerHeight;
  }

  startTracking = (e) => {
    // if you make an inline event listener (like onMouseDown), as you can see it's camelcase, so it is not the same
    // as the browser-native event onmousedown. The react's version of the event misses such properties like
    // offsetX and offsetY. These properties are position of the mouse in relation to the target - in this case canvas
    // and we need them to draw correctly. Under the hood of the react's event onMouseDown there still is access to
    // the browser-native event, we just have to refer to it in this way: e.nativeEvent. The native event has offsets.
    const { offsetX, offsetY } = e.nativeEvent;
    const { points, currentColor, currentStroke } = this.state;
    // getContext is a standard canvas-related method to indicate that drawing is in 2d
    const ctx = this.canvas.current.getContext('2d');
    const prevX = offsetX;
    const prevY = offsetY;
    // the canvas starts drawing the path in the place where the mouse was, during the initial pressing of the mouse button
    ctx.beginPath();
    ctx.moveTo(prevX, prevY);
    // we need to store this point as the first one in the state, to send it in the future to the db. the point needs to
    // not only have info about offsets and colors/strokewidth, but also about the mode, because we need to know, at which
    // point to execute which canvas method. Mode begin indicates, that this is where the user begun the drawing. Technically,
    // if we did not want to store the drawings in the db, we could not store the points at all.
    const point = {
      x: prevX,
      y: prevY,
      stroke: currentStroke,
      color: currentColor,
      mode: 'begin',
    };
    // we can not just save the points in the state, we need to first destructure whatever points already were in the state.
    this.setState({ points: [...points, point] });
    // we have to add another event dynamically, because if the user just moves the mouse over the canvas, without holding
    // down the button, the movement shouldn't be registered
    this.canvas.current.onpointermove = this.drawPoints;
  }

  drawPoints = (e) => {
    const {
      newX, newY, currentColor, currentStroke, points,
    } = this.state;
    // here we can destructure the offsets from the event without refering to the native event. We assined this event listener
    // dynamically via the vanila javascript statement and we reffered there to the browser-native event, not the React one.
    // You can recognize the difference by the lack of camelcase in the event name in the last line of the 'startTracking' function.
    // Since the assigned event is already a browser-native one, the offsets come with it automatically.
    const { offsetX, offsetY } = e;
    this.setState({ newX: offsetX, newY: offsetY });
    const ctx = this.canvas.current.getContext('2d');
    // lineTo is telling canvas where WILL be the lines, so between the points that we moved to after the beginPath method, and
    // these new points. This does not draw yet, it only informs the canvas where the drawing will be.
    ctx.lineTo(newX, newY);
    ctx.strokeStyle = currentColor;
    ctx.lineWidth = currentStroke;
    // the .stroke function is the one that actually draws. In here, this distinction is not so important for us to understand,
    // but it will be crucial for recreating the drawings from DB and to understand why do we use mode='draw' and mode='end' for points
    ctx.stroke();
    const point = {
      x: newX,
      y: newY,
      stroke: currentStroke,
      color: currentColor,
      mode: 'draw',
    };
    this.setState({ points: [...points, point] });
  }

  stopTracking = (e) => {
    // again the native event, for the same reason as with the onMouseDown
    const { offsetX, offsetY } = e.nativeEvent;
    const { currentColor, currentStroke, points } = this.state;
    // mode 'end' will mean, that after this final point, all the information about the points has been passed
    const point = {
      x: offsetX,
      y: offsetY,
      stroke: currentStroke,
      color: currentColor,
      mode: 'end',
    };
    // we need to reassign newX and newY to undefined, otherwise when we want to add more shapes/lines to the drawing,
    // it bugs out and starts the drawing at the same place where we finished the previous one, so we need a clean state
    this.setState({
      newX: undefined, newY: undefined, points: [...points, point],
    });
    // we need to cancel out the event registering the mouse movement over the canvas, when the user lets go of the button
    this.canvas.current.onpointermove = null;
  }

  reassignColor = (color) => {
    this.setState({ currentColor: color });
  }

  reassignStroke = (stroke) => {
    this.setState({ currentStroke: stroke });
  }

  // For saving canvas object in state
  // TODO: Should use setstate for this and maybe a setstate updater callback instead of forceupdate
  drawAll = () => {
    // the line below we will rewrite in to probably fetching of the drawing points from the db, but for now I have them in the
    // local storage to imitate them being stored somewhere
    const { points } = this.state;
    const ctx = this.canvas.current.getContext('2d');

    // this is where the magic happens. We're looping over all points and telling canvas to begin the path for the first point,
    // (the one with the mode begin), to assert where the lines go in the 'draw' mode and to finish and make the strokes in the 'end'
    // mode.
    points.forEach((point) => {
      if (point.x === null || point.y === null) return;
      if (point.mode === 'begin') {
        ctx.beginPath();
        ctx.moveTo(point.x, point.y);
      } else if (point.mode === 'draw') {
        ctx.lineTo(point.x, point.y);
        ctx.lineWidth = point.stroke;
        ctx.strokeStyle = point.color;
      } else if (point.mode === 'end') {
        ctx.stroke();
      }
    });
    this.forceUpdate();
  }

  clearCanvas = () => {
    const ctx = this.canvas.current.getContext('2d');
    if (window.confirm('Are you sure, you want to delete your drawing?')) {
      ctx.clearRect(0, 0, this.canvas.current.width, this.canvas.current.height);
      this.setState({ points: [] });
    }
  }

  onArrowBack = () => {
    const { onArrowBack } = this.props;
    onArrowBack();
  }

  render() {
    // * BaseClass for styling. To make unique classes. Named after compnent name
    // * Usign BEM (Block Element Modifier) naming aproach http://getbem.com/naming/
    // * fx <span class="block__elem"></span> / <div class="block block--mod">...</div>
    // * use - for longnames. eg. block-name__elem--mod / layout-nav__button--green
    const baseClass = 'canvas';
    const {
      allColors, currentColor, allStrokes, currentStroke,
    } = this.state;
    return (
      <div className={`${baseClass}-host`}>
        <h1>Draw a masterpiece</h1>
        <div className={`${baseClass}-container`}>
          <div className={`${baseClass}-color-buttons-container`}>
            <div className={`${baseClass}-stroke-container`}>
              {allStrokes.map((stroke, i) => (
                <button className={`${baseClass}-stroke-button`} key={`stroke-button-${stroke}`} style={{ width: `1.${stroke}9rem`, height: `1.${stroke}9rem`, backgroundColor: `${stroke === currentStroke ? '#F3B951' : 'black'}` }} id={i} onClick={() => this.reassignStroke(stroke)} type="button"> </button>
              ))}
            </div>
            {allColors.map((color, i) => (
              <button className={`${baseClass}-color-button ${color === currentColor ? 'active-color' : undefined}`} key={`color-button-${color}`} style={{ backgroundColor: color }} id={i} onClick={() => this.reassignColor(color)} type="button"> </button>
            ))}
          </div>
          <div className={`${baseClass}-drawing-items`}>
            <div className={`${baseClass}-buttons-container`}>
              <button onClick={this.clearCanvas} className={`${baseClass}-square-button`} type="button"><img src={DeleteIcon} alt="delete icon" /></button>
              <button className={`${baseClass}-square-button`} type="submit"><img src={SaveIcon} alt="save icon" /></button>
            </div>
            <canvas ref={this.canvas} width={0.7 * window.innerWidth} height={0.7 * window.innerHeight} onPointerDown={(e) => this.startTracking(e)} onPointerUp={(e) => this.stopTracking(e)} className={`${baseClass}-canvas`} />
          </div>
        </div>
      </div>
    );
  }
}

export default Canvas;

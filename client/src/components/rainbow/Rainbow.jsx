import React, { Component } from 'react';

// STYLES
import './rainbow.css';

class Rainbow extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  render() {
    // * BaseClass for styling. To make unique classes. Named after compnent name
    // * Usign BEM (Block Element Modifier) naming aproach http://getbem.com/naming/
    // * fx <span class="block__elem"></span> / <div class="block block--mod">...</div>
    // * use - for longnames. eg. block-name__elem--mod / layout-nav__button--green
    const baseClass = 'rainbow';

    return (
      <div className={`${baseClass}-container`}>
        <div className={`${baseClass}__element--color-1`} />
        <div className={`${baseClass}__element--color-2`} />
        <div className={`${baseClass}__element--color-3`} />
        <div className={`${baseClass}__element--color-4`} />
        <div className={`${baseClass}__element--color-5`} />
        <div className={`${baseClass}__element--color-6`} />
        <div className={`${baseClass}__element--color-7`} />
        <div className={`${baseClass}__element--color-8`} />
        <div className={`${baseClass}__element--color-9`} />
        <div className={`${baseClass}__element--color-10`} />
      </div>
    );
  }
}

export default Rainbow;

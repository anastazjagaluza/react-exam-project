// Reqular Expression Pattern for validation of email adresses
const validEmailRegex = RegExp(
  /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
);

// Validate input fields
const validateInputField = (target) => {
  const { name, value } = target;
  switch (name) {
    case 'email':
      if (!validEmailRegex.test(value)) {
        return {
          isValid: false,
          errMsg: 'Email is not valid',
        };
      }
      break;
    case 'password':
      // Check if the value contains at least:
      // One uppercase letter, one levercase letter, a length between 6-20 and at least one number
      if (!/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/.test(value)) {
        return {
          isValid: false,
          errMsg: 'Must contain one uppercase letter, one levercase letter, a length between 6-20 and at least one number',
        };
      }
      break;
    case 'confirmPassword':
      if (target.dataset.passwordToMatch !== value) {
        return {
          isValid: false,
          errMsg: 'Passwords must match',
        };
      }
      break;
    default:
      return {
        isValid: false,
        errMsg: 'Error. Form not validated',
      };
  }
  return {
    isValid: true,
    errMsg: '',
  };
};

// ValidateForm
// ? https://www.telerik.com/blogs/up-and-running-with-react-form-validation
const isFormValid = (formErrors, payload) => {
  let valid = true;
  Object.values(formErrors).forEach(
    // if we have an error string set valid to false
    (val) => {
      if (val.length > 0) {
        valid = false;
      }
    },
  );

  Object.values(payload).forEach(
    // if payload is null
    (val) => {
      if (val === null) {
        valid = false;
      }
    },
  );
  return valid;
};

export { validateInputField, isFormValid };

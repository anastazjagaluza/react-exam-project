const isAuthenticated = async () => {
  const accessToken = localStorage.getItem('accessToken');
  const payload = {
    accessToken,
  };
  const response = await fetch('auth/verifyToken', {
    headers: {
      Accept: 'application/json, text/plain, */*',
      'Content-Type': 'application/json',
    },
    method: 'POST',
    body: JSON.stringify(payload),
  });
  const result = await response;
  if (result.status !== 200) {
    console.log(result);
    console.log(result.status);
    console.log('ERROR IN FRONT');
    return false;
  }
  console.log(result.status);
  console.log('adddd');
  return true;
};

export default isAuthenticated;

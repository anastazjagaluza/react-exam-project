import React, { Component } from 'react';
import Canvas from '../../components/canvas/Canvas';
import Layout from '../../components/layout/Layout';

// STYLES
import './dashboard.css';

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userData: {
        email: 'Loading user data..',
      },
    };
  }

  componentDidMount = async () => {
    const accessToken = localStorage.getItem('accessToken');
    const payload = {
      accessToken,
    };
    const response = await fetch('/user', {
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify(payload),
    });

    const result = await response;
    if (result.status !== 200) {
      console.log(result.status);
      return;
    }
    const userData = await result.json();
    console.log(userData);
    this.setState({
      userData,
    });
  }

  render() {
    const { userData } = this.state;
    console.log(userData.email);
    return (
      <Layout>
        <h2>{userData.email}</h2>
        <Canvas />
      </Layout>
    );
  }
}

export default Dashboard;

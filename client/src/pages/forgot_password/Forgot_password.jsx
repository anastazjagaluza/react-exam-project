import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// SERVICES
import { validateInputField, isFormValid } from '../../services/validation';

// STYLES
import './forgot_password.css';

// ASSETS
import Logo1 from '../../assets/logos/Logo_1.svg';

// COMPONENTS
import Rainbow from '../../components/rainbow/Rainbow';

class ForgotPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: null,
      formSent: false,
      error: {
        email: '',
      },
    };
  }

  submit = async (payload) => {
    const response = await fetch('mailer/reset', {
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify(payload),
    });
    const result = await response;
    if (result.status !== 200) {
      console.log('ERROR: ', result.status);
    }
    console.log('SUCCES: ', result);
    // Set formSent to true to show dynamic UI msg to user
    this.setState({ formSent: true });
  }

      // HandleSubmit
      handleSubmit = (e) => {
        e.preventDefault();
        const { email, error } = this.state;
        console.log('Email:', email);
        const payload = {
          email,
        };
        if (isFormValid(error, payload)) {
          console.info('Valid Form');
          this.submit(payload);
        } else {
          console.error('Invalid Form');
          // TODO: HANDLE ERROR:
        }
      };

    // HandleChange
    // ? How to dunamically update nested state objects !
    // ? React has a hard time handling nested states, here is a way to do it
    // ? https://dev.to/walecloud/updating-react-nested-state-properties-ga6
    handleChange = (event) => {
      event.preventDefault();
      const { target } = event;
      const { name, value } = target;
      const { error } = { ...this.state };
      const currentErrorState = error;
      if (validateInputField(target).isValid) {
        currentErrorState[name] = '';
        this.setState({
          [name]: value,
          error: currentErrorState,
        });
      } else {
        currentErrorState[name] = validateInputField(target).errMsg;
        this.setState({
          [name]: value,
          error: currentErrorState,
        });
      }
    };

    render() {
    // * BaseClass for styling. To make unique classes. Named after compnent name
    // * Usign BEM (Block Element Modifier) naming aproach http://getbem.com/naming/
    // * fx <span class="block__elem"></span> / <div class="block block--mod">...</div>
    // * use - for longnames. eg. block-name__elem--mod / layout-nav__button--green
      const baseClass = 'forgot_password';

      const { error, formSent } = this.state;

      return (
        <div className={`${baseClass}__content`}>
          <h1 className={`${baseClass}__h1`}>Forgot password?</h1>
          <p className={`${baseClass}__p`}>
            If you can’t remember you password provide us wi
            th the email connected to your account and we will send you
            an email with a link to reset the your password. Please check
            your spam folder.
          </p>
          <form className="form" noValidate onSubmit={this.handleSubmit}>
            <label className={`form__label ${error.email.length > 0 && '--not-valid'}`} htmlFor="email">
              Email:
              <input className="form__input" noValidate type="email" name="email" placeholder="Email" onChange={this.handleChange} />
              <div className="form__error-msg">{error.email}</div>
            </label>
            <div className={`${baseClass}__succes ${formSent && '--form_sent'}`}>Succes! Check your email.</div>
            <button className="form__button--orange" type="submit">Send Email</button>
            <div className={`${baseClass}__actions-container`}>
              <Link className="form__link text--center" to="/">Login</Link>
              <Link className="form__link text--center" to="/signup">Sign Up</Link>
            </div>
          </form>
          <Rainbow />
          <img className={`${baseClass}__logo`} src={Logo1} alt="Logo" />
        </div>
      );
    }
}

export default ForgotPassword;

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// SERVICES
import { validateInputField, isFormValid } from '../../services/validation';

// STYLES
import './login.css';

// ASSETS
import Logo1 from '../../assets/logos/Logo_1.svg';

// COMPONENTS
import Rainbow from '../../components/rainbow/Rainbow';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: null,
      password: null,
      error: {
        email: '',
        password: '',
      },
    };
  }

  submit = async (payload) => {
    const response = await fetch('auth/login', {
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify(payload),
    });
    const result = await response;
    if (result.status !== 200) {
      console.log(result);
      console.log(result.status);
      console.log('ERROR IN FRONT');
      return;
    }

    console.log(result);
    console.dir(result.headers);
    console.dir(result.headers.get('AccessToken'));
    console.dir(result.headers.get('accessToken'));
    console.log('User Was Auth!');
    // TODO: Store AccessToken in localSTr
    const accessToken = result.headers.get('AccessToken');
    localStorage.setItem('accessToken', accessToken);

    // Get history from props passed from public route higher-order component
    const { history } = this.props;
    history.push('/dashboard');
  }

    // HandleSubmit
    handleSubmit = (e) => {
      e.preventDefault();
      const { email, password, error } = this.state;
      console.log('Email:', email);
      const payload = {
        email,
        password,
      };
      if (isFormValid(error, payload)) {
        console.info('Valid Form');
        // * SEND FORM DATA TO API
        this.submit(payload);
      } else {
        console.error('Invalid Form');
        // TODO: HANDLE ERROR:
      }
    };

  // HandleChange
  // ? How to dunamically update nested state objects !
  // ? React has a hard time handling nested states, here is a way to do it
  // ? https://dev.to/walecloud/updating-react-nested-state-properties-ga6
  handleChange = (event) => {
    event.preventDefault();
    const { target } = event;
    const { name, value } = target;
    const { error } = { ...this.state };
    const currentErrorState = error;
    if (validateInputField(target).isValid) {
      currentErrorState[name] = '';
      this.setState({
        [name]: value,
        error: currentErrorState,
      });
    } else {
      currentErrorState[name] = validateInputField(target).errMsg;
      this.setState({
        [name]: value,
        error: currentErrorState,
      });
    }
  };

  render() {
    // * BaseClass for styling. To make eunique classes. Named after compnent name
    // ? Usign BEM (Block Element Modifier) naming aproach http://getbem.com/naming/
    // ? fx <span class="block__elem"></span> / <div class="block block--mod">...</div>
    // ? use - for longnames. eg. block-name__elem--mod / layout-nav__button--green
    const baseClass = 'login';

    const { error } = this.state;

    return (
      <div className={`${baseClass}__content`}>
        <img className={`${baseClass}__logo`} src={Logo1} alt="Logo" />
        <Rainbow />
        <form className="form" noValidate onSubmit={this.handleSubmit}>
          <label className={`form__label ${error.email.length > 0 && '--not-valid'}`} htmlFor="email">
            Email:
            <input className="form__input" noValidate type="email" name="email" placeholder="Email" onChange={this.handleChange} />
            <div className="form__error-msg">{error.email}</div>
          </label>
          <label className={`form__label ${error.password.length > 0 && '--not-valid'}`} htmlFor="password">
            Password:
            <input className="form__input" noValidate type="password" name="password" placeholder="Password" onChange={this.handleChange} />
            <div className="form__error-msg">{error.password}</div>
          </label>
          <Link className="form__link text--right" to="/forgot-password">Forgot Password?</Link>
          <button className="form__button--green" type="submit">Login</button>
          <Link className="form__link text--center" to="/signup">Sign Up</Link>
        </form>
      </div>
    );
  }
}

export default Login;

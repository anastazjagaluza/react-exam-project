import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// SERVICES
import { validateInputField, isFormValid } from '../../services/validation';

// STYLES
import './signup.css';

// ASSETS
import Logo1 from '../../assets/logos/Logo_1.svg';

// COMPONENTS
import Rainbow from '../../components/rainbow/Rainbow';

class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: null,
      password: null,
      confirmPassword: null,
      error: {
        email: '',
        password: '',
        confirmPassword: '',
      },
    };
  }

  submit = async (payload) => {
    const response = await fetch('user/create', {
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify(payload),
    });
    const result = await response;
    if (result.status !== 200) {
      console.log(result);
      console.log(result.status);
      console.log('ERROR IN FRONT');
      return;
    }
    const accessToken = result.headers.get('AccessToken');
    localStorage.setItem('accessToken', accessToken);

    console.log('User Was Created!');

    // Get history from props passed from public route higher-order component
    const { history } = this.props;
    history.push('/dashboard');
  }

  // HandleSubmit
  handleSubmit = (e) => {
    e.preventDefault();
    const { email, password, error } = this.state;
    const payload = {
      email,
      password,
    };
    if (isFormValid(error, payload)) {
      console.info('Valid Form');
      // * SEND FORM DATA TO API
      this.submit(payload);
    } else {
      console.error('Invalid Form');
      // TODO: HANDLE ERROR:
    }
  };

  // HandleChange
  // ? How to dunamically update nested state objects !
  // ? React has a hard time handling nested states, here is a way to do it
  // ? https://dev.to/walecloud/updating-react-nested-state-properties-ga6
  handleChange = (event) => {
    event.preventDefault();
    const { target } = event;
    const { name, value } = target;
    const { error } = { ...this.state };
    const currentErrorState = error;
    if (validateInputField(target).isValid) {
      currentErrorState[name] = '';
      this.setState({
        [name]: value,
        error: currentErrorState,
      });
    } else {
      currentErrorState[name] = validateInputField(target).errMsg;
      this.setState({
        [name]: value,
        error: currentErrorState,
      });
    }
  };

  render() {
    // * BaseClass for styling. To make eunique classes. Named after compnent name
    // ? Usign BEM (Block Element Modifier) naming aproach http://getbem.com/naming/
    // ? fx <span class="block__elem"></span> / <div class="block block--mod">...</div>
    // ? use - for longnames. eg. block-name__elem--mod / layout-nav__button--green
    const baseClass = 'signup';

    const { error, password } = this.state;

    return (
      <div className={`${baseClass}__content`}>
        <img className={`${baseClass}__logo`} src={Logo1} alt="Logo" />
        <Rainbow />
        <form className="form" noValidate onSubmit={this.handleSubmit}>
          <label className={`form__label ${error.email.length > 0 && '--not-valid'}`} htmlFor="email">
            Email:
            <input className="form__input" noValidate type="email" name="email" placeholder="Email" onChange={this.handleChange} />
            <div className="form__error-msg">{error.email}</div>
          </label>
          <label className={`form__label ${error.password.length > 0 && '--not-valid'}`} htmlFor="password">
            Password:
            <input className="form__input" noValidate type="password" name="password" placeholder="Password" onChange={this.handleChange} />
            <div className="form__error-msg">{error.password}</div>
          </label>
          <label className={`form__label ${error.confirmPassword.length > 0 && '--not-valid'}`} htmlFor="confirmPassword">
            Confirm Password:
            <input className="form__input" noValidate type="password" data-password-to-match={password} name="confirmPassword" placeholder="Confirm Password" onChange={this.handleChange} />
            <div className="form__error-msg">{error.confirmPassword}</div>
          </label>
          <button className="form__button--orange" type="submit">Sign Up</button>
          <Link className="form__link text--center" to="/">Login</Link>
        </form>
      </div>
    );
  }
}

export default Signup;

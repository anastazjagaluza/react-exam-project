import React, { Component } from 'react';

// SERVICES
import { validateInputField, isFormValid } from '../../services/validation';

// STYLES
import './reset_password.css';

class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: null,
      confirmPassword: null,
      error: {
        password: '',
        confirmPassword: '',
      },
    };
  }

  submit = async (payload, id) => {
    // We need to pass the id from the query string to verify it is the correct user
    const response = await fetch(`auth/reset-password?id=${id}`, {
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify(payload),
    });
    const result = await response;
    if (result.status !== 200) {
      console.log('ERROR: ', result.status);
      return;
    }
    console.log(result);
    // Get history from props passed from public route higher-order component
    const { history } = this.props;
    history.push('/');
  }

  // HandleSubmit
  handleSubmit = (e) => {
    e.preventDefault();
    const { password, error } = this.state;
    // Get query string id value and save it in a const
    const query = new URLSearchParams(window.location.search);
    const id = query.get('id');
    console.log('ID: ', id);

    const payload = {
      password,
    };
    if (isFormValid(error, payload)) {
      console.info('Valid Form');
      // Pass payload and id to submit func
      this.submit(payload, id);
    } else {
      console.error('Invalid Form');
      // TODO: HANDLE ERROR:
    }
  };

  handleChange = (event) => {
    event.preventDefault();
    const { target } = event;
    const { name, value } = target;
    const { error } = { ...this.state };
    const currentErrorState = error;
    if (validateInputField(target).isValid) {
      currentErrorState[name] = '';
      this.setState({
        [name]: value,
        error: currentErrorState,
      });
    } else {
      currentErrorState[name] = validateInputField(target).errMsg;
      this.setState({
        [name]: value,
        error: currentErrorState,
      });
    }
  };

  render() {
    const baseClass = 'reset_password';

    const { error, password } = this.state;

    return (
      <div className={`${baseClass}__content`}>
        <h1 className={`${baseClass}__h1`}>Change password</h1>
        <p className={`${baseClass}__p`}>
          In order to protect your account,
          make sure your password is atleast 6 characters or longer.
          That it contains atleast a lowercase and uppercase letter and atleast a number.
        </p>
        <form className="form" noValidate onSubmit={this.handleSubmit}>
          <label className={`form__label ${error.password.length > 0 && '--not-valid'}`} htmlFor="password">
            Password:
            <input className="form__input" noValidate type="password" name="password" placeholder="Password" onChange={this.handleChange} />
            <div className="form__error-msg">{error.password}</div>
          </label>
          <label className={`form__label ${error.confirmPassword.length > 0 && '--not-valid'}`} htmlFor="confirmPassword">
            Confirm Password:
            <input className="form__input" noValidate type="password" data-password-to-match={password} name="confirmPassword" placeholder="Confirm Password" onChange={this.handleChange} />
            <div className="form__error-msg">{error.confirmPassword}</div>
          </label>
          <button className="form__button--orange" type="submit">Reset password</button>
        </form>
      </div>
    );
  }
}

export default ResetPassword;

import React, { Component } from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';

// STYLES
import './styles/reset/reset.css';
import './styles/colors/color_palette.css';
import './styles/fonts/fonts.css';
import './App.css';

// TODO: Add Private and public routes
import ProtectedRoute from './routes/Protected.route';
import PublicRoute from './routes/Public.route';

// PAGES
import LOGIN from './pages/login/LogIn';
import SIGNUP from './pages/signup/SignUp';
import FORGOT_PASSWORD from './pages/forgot_password/Forgot_password';
import DASHBOARD from './pages/dashboard/Dashboard';
import RESET_PASSWORD from './pages/reset_password/Reset_password';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  // * ProtectedRoute only accesible when logged in
  // * PublicRoute only accesible when 'NOT' logged in

  render() {
    return (
      <Router>
        <Switch>
          <PublicRoute exact path="/" component={LOGIN} />
          <PublicRoute exact path="/signup" component={SIGNUP} />
          <PublicRoute exact path="/forgot-password" component={FORGOT_PASSWORD} />
          <PublicRoute exact path="/reset-password" component={RESET_PASSWORD} />
          <ProtectedRoute exact path="/dashboard" component={DASHBOARD} />
        </Switch>
      </Router>
    );
  }
}

export default App;

// ? TUTORIALS. SOURCE OF KNOWLEDGE
// ? ReactRouter https://www.youtube.com/watch?v=Law7wfdg_ls&t=1713s
// ? PrivateRutes https://www.youtube.com/watch?v=Y0-qdp-XBJg

// EKSAMPLE TEMPLATE
// import React, { Component } from 'react'

// class App extends Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//     };
//   }

//   render() {
//     return (
//       <div>
//         <h1>Hello World</h1>
//       </div>
//     );
//   }
// }

// export default App;

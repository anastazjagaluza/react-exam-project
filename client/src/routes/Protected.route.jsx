// EKSAMPLE TEMPLATE
import React from 'react';
import { withRouter } from 'react-router-dom';

// SERVICES
import isAuthenticated from '../services/auth';

class ProtectedRoute extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isAuth: false,
    };
  }

  componentDidMount = async () => {
    console.log(this.props);
    const isAuth = await isAuthenticated();

    if (isAuth) {
      console.log('Auth is true');
      this.setState({
        isAuth: true,
      });
    } else {
    // Redirect
      console.log('Auth is feals');
      const { history } = this.props;
      history.push('/');
    }
  }

  render() {
    const { component: Component } = this.props;
    console.log(isAuthenticated);

    const { isAuth } = this.state;

    return isAuth ? (
      <Component />
    ) : (
      <div>
        <h1>Loading</h1>
      </div>
    );
  }
}

// ! High Oder Component
// ? https://reactjs.org/docs/higher-order-components.html
export default withRouter(ProtectedRoute);

// ! SOURCE OF KNOWLEDGE
// ? https://medium.com/javascript-in-plain-english/how-to-set-up-protected-routes-in-your-react-application-a3254deda380

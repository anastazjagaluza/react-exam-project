// EKSAMPLE TEMPLATE
import React from 'react';
import { Redirect, withRouter } from 'react-router-dom';

class PublicRoute extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  render() {
    const { history, component: Component } = this.props;
    const isAuthenticated = true; // TODO: CALL ATUENTICATION LOGIC HERE

    return isAuthenticated ? (
      <Component history={history} />
    ) : (
      <Redirect to={{ pathname: '/dashboard' }} />
    );
  }
}

export default withRouter(PublicRoute);

// ! SOURCE OF KNOWLEDGE
// ? https://medium.com/javascript-in-plain-english/how-to-set-up-protected-routes-in-your-react-application-a3254deda380

const jwt = require('jsonwebtoken');
require('dotenv').config();

// this function also generates a token for particular user,
// but it uses another key on our side for encryption and future decryption
// the purpose of refresh token is to be able in the future check whether
// the user is allowed to generate a new
// access token after this one is expired.
// All of this might seem complex, but it's very secure this way.
const generateRefreshToken = (user) => jwt.sign(user, process.env.REFRESH_TOKEN);
// REFRESH_TOKEN='662c3fa072062cf3231f8f446c48ca6d9a7fbab1cb295f0143e1903c4f7d89747d8d1fd1b7cd2d946bc2162275a5134edb1b7075f2a8ab926f1975d65916265b'

module.exports = generateRefreshToken;

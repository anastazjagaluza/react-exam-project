const jwt = require('jsonwebtoken');

require('dotenv').config();

// jwt creates a special access for a particular user, that is based on our secret key
// (stored in .env) which is our base
// for encryption and decryption of the token assigned to the user.
// Access token will expire in 15m (it doesn't have to, but I decided we can
// do it this way to make it super secure)
const generateAccessToken = (user) => jwt.sign(user, process.env.ACCESS_TOKEN, { expiresIn: '15m' });
// ACCESS_TOKEN='2b7545223c94d13fafa7af45213290d8f2134426e8ce503a1529c6a1c7ec2f19ae7aa9cfec2845ffc8e4f367ebebfb9065c21e90a7cea12d84480074cb3d0ae5'
module.exports = generateAccessToken;

// This function takes to arguments.
// The value and the type of value fx. "john@doe.com", "email"
const validate = (value, valueType) => {
  // Set bool default as false
  let isValid = false;

  // Check if the string is empty or undefined
  if (value === '' || undefined) {
    console.log('missing values');
    isValid = false;
    return isValid;
  }
  // Check the length of the value is below 50 chars
  if (value.length > 50) {
    console.log('too long');
    isValid = false;
    return isValid;
  }
  // Check if the valuetype is email and the passed string value is an email w. Regex
  if (valueType === 'email' && !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value)) {
    console.log('Field not an email');
    isValid = false;
    return isValid;
  }
  // Check if the valuetype is password and the passed string value contains at least
  // One uppercase letter, one levercase letter, a length between 6-20 and at least one number
  if (valueType === 'password' && !/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/.test(value)) {
    console.log('More strong password');
    isValid = false;
    return isValid;
  }
  // If value is correct set isValid bool to true and return bool value
  isValid = true;
  // Return bool
  return isValid;
};

module.exports = validate;

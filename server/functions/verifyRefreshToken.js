const jwt = require('jsonwebtoken');

// REFRESH_TOKEN='662c3fa072062cf3231f8f446c48ca6d9a7fbab1cb295f0143e1903c4f7d89747d8d1fd1b7cd2d946bc2162275a5134edb1b7075f2a8ab926f1975d65916265b'
const refreshTokenVerified = (refreshToken) => {
  // if the access code has expired, user has to login again
  // and get a new access code, but before that, we need to
  // verify if user has a correct refresh token, which was
  // generated on a completely different key from our dotenv file
  const response = jwt.verify(refreshToken, process.env.REFRESH_TOKEN, (err, decoded) => {
    if (err) return null;
    return decoded.email;
  });
  return response;
};

module.exports = refreshTokenVerified;

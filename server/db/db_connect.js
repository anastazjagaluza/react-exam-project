const { MongoClient } = require('mongodb');

require('dotenv').config();
// DB_CONNECTION_URL=mongodb+srv://admin:admin123@reactcluster.ve2p4.mongodb.net/react-exam?retryWrites=true&w=majority
const dbConnectionUrl = process.env.DB_CONNECTION_URL;

const client = new MongoClient(dbConnectionUrl, { useUnifiedTopology: true });

client.connect((error) => {
  if (!error) {
    return 'Connected';
  }
  return error;
});

module.exports = client;

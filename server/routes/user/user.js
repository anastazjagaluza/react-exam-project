const express = require('express');

const router = express.Router();

// CONTROLLERS
const getUser = require('../../controllers/user/getUser');
const createUser = require('../../controllers/user/createUser');

// Routes
router
  .post('/', getUser) // GET SINGLE USER
  .post('/create', createUser); // CREATE USER

module.exports = router;

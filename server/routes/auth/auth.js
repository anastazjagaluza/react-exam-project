const express = require('express');

const router = express.Router();

// CONTROLLERS
const resetPassword = require('../../controllers/auth/resetPassword');
const login = require('../../controllers/auth/login');
const verifyToken = require('../../controllers/auth/verifyToken');

// Routes
router
  .post('/reset-password', resetPassword) // UPDATE USER PASSWORD BY RESET TOKEN ID
  .post('/login', login) // UPDATE USER PASSWORD BY RESET TOKEN ID
  .post('/verifyToken', verifyToken); // VERIFY USER FROM ACCESSTOKEN

module.exports = router;

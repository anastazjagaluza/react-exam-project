const express = require('express');

const router = express.Router();

// CONTROLLERS
const resetPasswordMail = require('../../controllers/mailer/resetPasswordMail.js');

// Routes
router
  .post('/reset', resetPasswordMail); // Reset password with email

module.exports = router;

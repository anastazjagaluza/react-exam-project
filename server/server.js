// IMPORT MODULES
require('dotenv').config();
const express = require('express');
const path = require('path');

const app = express(); // express app

const PORT = process.env.PORT || 9000; // SET PORT CONFIG

// ALLOW JSON BODY PARSER
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Serve static files from the client React build folder
app.use(express.static(path.join(path.resolve(__dirname, '..'), 'client/build')));

// USER ROUTES
const user = require('./routes/user/user');

app.use('/user', user);

// MAILER ROUTES
const mailer = require('./routes/mailer/mailer');

app.use('/mailer', mailer);

// AUTH ROUTES
const Auth = require('./routes/auth/auth');

app.use('/auth', Auth);

// SEND ALL REQUEST WHICH DOES NOT MATCH TO CLIENT REACT BIILD INDEX FILE
// 404-page is handled in react-frontend-clientside
app.get('*', (req, res) => {
  res.sendFile(path.join(path.resolve(__dirname, '..'), '/client/build/index.html'));
});

// LISTEN
app.listen(PORT, (err) => {
  if (err) {
    return console.log('ERROR', err);
  }
  console.log(`Listening on port ${PORT}`);
});

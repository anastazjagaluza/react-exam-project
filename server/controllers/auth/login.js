// Require bcrypt for encrypting password
const bcrypt = require('bcrypt');

const client = require('../../db/db_connect.js');

// Import validate function
const validate = require('../../functions/validation');

// import token validation for jwt purposes, read the file for more explanation
const refreshTokenVerified = require('../../functions/verifyRefreshToken');
const generateAccessToken = require('../../functions/generateAccessToken');

const login = (req, res) => {
  const { email, password } = req.body;

  // Check if values are undefined and not passed
  if (!email || !password) {
    return res.sendStatus(400);
  }
  // Use custom validation func to check if email and password value is correct.
  // Remember to pass both value and valuetype
  if (!validate(email, 'email')) {
    return res.sendStatus(400);
  }
  if (!validate(password, 'password')) {
    return res.sendStatus(400);
  }

  // Connect to Mongodb atlas db
  client.connect((err) => {
    if (err) throw err;

    // Set a collection variable for the USERS collection
    const collection = client.db('react-exam').collection('users');

    // Find user with email from post req body
    collection.findOne({ email }, (findError, result) => {
      if (result) {
        // Compare password from post req body with mongodb and check for match
        bcrypt.compare(password, result.password, (compareError, isMatch) => {
          // If error occurs while comparing w. bcrypt
          if (compareError || !isMatch) {
            return res.sendStatus(404);
          }

          // If everything went correctly until now, it's time to
          // generate a new access token, but first we need to
          // retrieve a refresh token and check if its
          // correct according to our key.
          const decodedEmail = refreshTokenVerified(result.refreshToken);
          if (decodedEmail == null || decodedEmail !== email) {
            return res.status(403);
          }

          const accessToken = generateAccessToken({ email });

          return res.set('AccessToken', accessToken).sendStatus(200);
        });
      }
      // If email does not match anything
      if (!result) {
        return res.sendStatus(404);
      }
      // Error on findOne method on collection
      if (findError) {
        return res.sendStatus(500);
      }
    });
  });
};

module.exports = login;

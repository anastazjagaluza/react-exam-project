// ** THIS ENDPOINT UPDATES THE USERS PASSWORD
// Require bcrypt for encrypting password
const bcrypt = require('bcrypt');

const client = require('../../db/db_connect.js');

// Import validate function
const validate = require('../../functions/validation');

const resetPassword = (req, res) => {
  // Deconstruct request body and query to use password and id
  const { password } = req.body;
  const { id } = req.query;
  console.log('ID: ', id);

  // Check if password is undefined and not passed
  if (!password) {
    return res.sendStatus(400);
  }
  // Use custom validation func to check if password value is correct.
  // Remember to pass both value and valuetype
  if (!validate(password, 'password')) {
    return res.sendStatus(400);
  }

  // Connect to Mongodb atlas db
  client.connect((err) => {
    if (err) throw err;
    // Set a collection variable for the USERS collection
    const collection = client.db('react-exam').collection('users');

    // Query for mongodb UPDATE with "query string reset token id" for finding user to update
    const query = { reset_token: id };

    // Hash new password to pass to db
    bcrypt.hash(password, 10, (error, hash) => {
      if (error) throw error;
      console.log('HASH: ', hash);

      // newValuesObject for password and reseting token id
      const newValuesObject = { $set: { password: hash, reset_token: 0 } };

      // Update user with new password and set reset token id to 0
      collection.updateOne(query, newValuesObject, (findError, result) => {
        if (result) {
          return res.status(200).send(result);
        }
        // If email does not match anything
        if (!result) {
          return res.sendStatus(404);
        }
        // Error on findOne method on collection
        if (findError) {
          return res.sendStatus(500);
        }
      });
    });
  });
};

module.exports = resetPassword;

// ** DOCS: https://nodemailer.com/about/
// ** Read this if you need a better understanding

// Require nodemailer
const nodemailer = require('nodemailer');

// Require dotenv for configuring the transporter auth object
require('dotenv').config();

// async..await is not allowed in global scope, must use a wrapper
// Takes email and id for content of email from nodemailer
async function resetPassword(email, id, req) {
  // create reusable transporter object using the outlook SMTP transport
  // ** Resource: https://ourcodeworld.com/articles/read/264/how-to-send-an-email-gmail-outlook-and-zoho-using-nodemailer-in-node-js
  // ** Read this to see how to set up transport for different email providers
  const transporter = nodemailer.createTransport({
    host: 'smtp-mail.outlook.com',
    port: 587,
    secure: false, // true for 465, false for other ports
    tls: {
      ciphers: 'SSLv3',
    },
    auth: {
      user: process.env.MAILER_EMAIL, // .env email information
      pass: process.env.MAILER_PASSWORD, // .env password for outlook mail info
    },
  });

  // CHANGE URL PATH DEPENDING ON PROD OR DEVELOPMENT
  const devResetUserPasswordApiURL = `http://localhost:3000/reset-password?id=${id}`;
  // Sets relative Url to match PROD env
  const prodResetUserPasswordApiURL = `${req.protocol}://${req.get('host')}/reset-password?id=${id}`;

  const resetUserPasswordApiURL = (process.env.DEV_OR_PROD === 'PRODUCTION') ? prodResetUserPasswordApiURL : devResetUserPasswordApiURL;

  // Send mail with defined transport object
  const info = await transporter.sendMail({
    from: '"Skatcher" <skatcher.exam@outlook.dk>', // sender address
    to: `${email}`, // list of receivers
    subject: 'Hello ✔ Here is a link to reset your password.', // Subject line
    text: `Hi, ${email}.`, // plain text body
    html: `Hi, <b>${email}.</b><br/><br/>If you meant to reset your password follow the instructions. <br/><br/>Otherwise If you did not perform this request, you can safely ignore this email and contact us for guidance.<br/><br/><a href='${resetUserPasswordApiURL}'>Click here to reset password.</a>`, // html body
  });

  console.log('Message sent: %s', info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
}

module.exports = resetPassword;

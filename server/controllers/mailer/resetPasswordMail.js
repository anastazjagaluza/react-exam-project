// ** THIS ENDPOINT SENTS THE EMAIL WITH THE LINK TO RESET PASSWORD
// Use UUID to generate random id for reset token
const { v4: uuidv4 } = require('uuid');

// Require function with nodemailer functionallity
const resetPasswordFunc = require('./functions/resetpassword.nodemailer');

const client = require('../../db/db_connect.js');

const resetPasswordMail = (req, res) => {
  const { email } = req.body;

  // Check if email is undefined and not passed
  if (!email) {
    return res.sendStatus(400);
  }

  // Connect to Mongodb atlas db
  client.connect((err) => {
    if (err) throw err;
    // Set a collection variable for the USERS collection
    const collection = client.db('react-exam').collection('users');

    // Query for Mongodb UPDATE with email for finding user to update and add reset token id
    const query = { email: req.body.email };

    // Reset token with random generated id uuidv4
    const resetToken = { $set: { reset_token: uuidv4() } };
    console.log('TOKEN: ', resetToken.$set.reset_token);

    // Find user with passed email and update with resetToken
    // for authenticating, that they can change password
    collection.updateOne(query, resetToken, (findError, result) => {
      if (result) {
        // Function for handling sending a mail with nodemailer
        // Takes two args of email and query string reset token id
        resetPasswordFunc(req.body.email, resetToken.$set.reset_token, req).catch((error) => console.log('ERROR:', error));
        // Return OK
        return res.status(200).json(result);
      }
      // If email does not match anything
      if (!result) {
        return res.sendStatus(404);
      }
      // Error on findOne method on collection
      if (findError) {
        return res.sendStatus(500);
      }
    });
  });
};

module.exports = resetPasswordMail;

// Require bcrypt for encrypting password
const bcrypt = require('bcrypt');

const client = require('../../db/db_connect.js');
const generateAccessToken = require('../../functions/generateAccessToken');
const generateRefreshToken = require('../../functions/generateRefreshToken');

// Import validate function
const validate = require('../../functions/validation');

const createUser = (req, res) => {
  const { email, password } = req.body;

  // Check if values are undefined and not passed
  if (!email || !password) {
    return res.sendStatus(400);
  }
  // Use custom validation func to check if email and password value is correct.
  // Remember to pass both value and valuetype
  if (!validate(email, 'email')) {
    return res.sendStatus(400);
  }
  if (!validate(password, 'password')) {
    return res.sendStatus(400);
  }
  console.log('My MANNE!');
  // generating access token and refresh token for later for when the access token expires.
  const accessToken = generateAccessToken({ email });
  const refreshToken = generateRefreshToken({ email });
  // Connect to Mongodb atlas db
  client.connect((err) => {
    if (err) throw err;
    // Set a collection variable for the USERS collection
    const collection = client.db('react-exam').collection('users');

    // Use bcrypt to hash password and add to object
    bcrypt.hash(password, 10, (error, hash) => {
      if (error) throw error;

      // the refresh token gets inserted to the database for later reference
      const secureUserObj = {
        email,
        password: hash,
        refreshToken,
      };
      // Insert the object created in the bcrypt method and return ok
      collection.insertOne(secureUserObj, (insertError) => {
        // Insert was OK
        if (!insertError) {
          // we pass access token in headers to store it in local storage
          return res.set('AccessToken', accessToken).sendStatus(200);
        }
        // Insert was not OK
        if (insertError) {
          return res.sendStatus(500);
        }
      });
    });
  });
};

module.exports = createUser;

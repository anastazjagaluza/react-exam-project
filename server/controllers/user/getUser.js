const jwt = require('jsonwebtoken');

require('dotenv').config();

const client = require('../../db/db_connect.js');

const getUser = (req, res) => {
  // Find access token from POST body
  const { accessToken } = req.body;
  // Check if access token is null
  if (accessToken === null) {
    res.sendStatus(400);
  }

  // Verify accessToken from headers
  const isVerified = jwt.verify(accessToken, process.env.ACCESS_TOKEN, (err) => {
    if (err) return false;
    return true;
  });

  // Check if not verified then return access forbidden
  if (!isVerified) {
    return res.sendStatus(403);
  }
  // Decode access token to get email from payload
  const decodedToken = jwt.decode(accessToken);

  // deconstruct email from decodedToken constant
  const { email } = decodedToken;

  // Connect to Mongodb atlas db
  client.connect((err) => {
    if (err) throw err;

    // Set a collection variable for the USERS collection
    const collection = client.db('react-exam').collection('users');

    // eslint-disable-next-line quote-props
    const query = { 'email': email };
    // Projection to scope what we want to get from result
    const projection = { projection: { _id: 0, email: 1 } };

    // Find user with email from post req body
    collection.findOne(query, projection, (findError, result) => {
      if (result) {
        console.log(result);
        return res.status(200).send(result);
      }
      // If id does not match anything
      if (!result) {
        return res.sendStatus(404);
      }
      // Error on findOne method on collection
      if (findError) {
        return res.sendStatus(500);
      }
    });
  });
};

module.exports = getUser;

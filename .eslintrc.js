module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 12,
  },
  rules: {
    'no-console': 'off', // ? Maybe remove before handin / production
    'consistent-return': 'off',
    'import/no-extraneous-dependencies': 'off',
    'max-len': 'off', // added so that it wouldn't complain about the long comments, where we pass out .env variables to each other
    'import/no-unresolved': [
      2,
      { caseSensitive: false },
    ],
  },
};
